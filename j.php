<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require 'theme_pagesuite/framework.php';
    require 'theme_pagesuite/php_helper.php';
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jameson Page</title>
</head>
<body>
    <?php
    pagesuite_card1([
        "id" => "uniqId",
        "class" => "genericClass",
        "breakpoint" => "500, 1000, 1500",
        "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
        "img_src" => "img/card1b.svg",
        "img_alt" => "Image alt text",
        "header" => "EDITION",
        "caption" => "Crafted editions developed using existing content, offering publishers new way to grow revenue",
    ]);
    ?>
</body>
</html>