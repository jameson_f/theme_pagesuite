<?php

// ! red comment
// * greenish comment
// ? blue comment
// todo:

// JavaScript execution hierarchy:
// async or the first time it gets parsed
// defer
// document - DOMContentLoaded
// window - onload

/* ----------------------------------------------------------------------------------------------------------------------- */

echo "<link rel='stylesheet' href='/theme_pagesuite/style.css'>";
echo "<script src='/theme_pagesuite/script.js'></script>";
require 'php_helper.php';

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_logo($params = null) {
    echo "
        <figure pagesuite logo
            ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
            ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
            ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
            ".($params['attr_addons'] ?? NULL)."
        >
            <img
            src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
            ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
            />
            <svg width='153' height='35' viewBox='0 0 153 35' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path d='M8.90892 14.58C9.90492 14.58 10.7809 14.448 11.5369 14.184C12.3049 13.92 12.9469 13.554 13.4629 13.086C13.9909 12.606 14.3869 12.036 14.6509 11.376C14.9149 10.716 15.0469 9.99004 15.0469 9.19804C15.0469 7.55404 14.5369 6.27004 13.5169 5.34604C12.5089 4.42204 10.9729 3.96004 8.90892 3.96004H4.76892V14.58H8.90892ZM8.90892 1.20604C10.5409 1.20604 11.9569 1.39804 13.1569 1.78204C14.3689 2.15404 15.3709 2.68804 16.1629 3.38404C16.9549 4.08004 17.5429 4.92004 17.9269 5.90404C18.3229 6.88804 18.5209 7.98604 18.5209 9.19804C18.5209 10.398 18.3109 11.496 17.8909 12.492C17.4709 13.488 16.8529 14.346 16.0369 15.066C15.2329 15.786 14.2309 16.35 13.0309 16.758C11.8429 17.154 10.4689 17.352 8.90892 17.352H4.76892V27H1.29492V1.20604H8.90892Z' stroke='white'/>
                <path d='M31.4795 18.792C30.0035 18.84 28.7435 18.96 27.6995 19.152C26.6675 19.332 25.8215 19.572 25.1615 19.872C24.5135 20.172 24.0395 20.526 23.7395 20.934C23.4515 21.342 23.3075 21.798 23.3075 22.302C23.3075 22.782 23.3855 23.196 23.5415 23.544C23.6975 23.892 23.9075 24.18 24.1715 24.408C24.4475 24.624 24.7655 24.786 25.1255 24.894C25.4975 24.99 25.8935 25.038 26.3135 25.038C26.8775 25.038 27.3935 24.984 27.8615 24.876C28.3295 24.756 28.7675 24.588 29.1755 24.372C29.5955 24.156 29.9915 23.898 30.3635 23.598C30.7475 23.298 31.1195 22.956 31.4795 22.572V18.792ZM21.1295 11.34C22.1375 10.368 23.2235 9.64204 24.3875 9.16204C25.5515 8.68204 26.8415 8.44204 28.2575 8.44204C29.2775 8.44204 30.1835 8.61004 30.9755 8.94604C31.7675 9.28204 32.4335 9.75004 32.9735 10.35C33.5135 10.95 33.9215 11.676 34.1975 12.528C34.4735 13.38 34.6115 14.316 34.6115 15.336V27H33.1895C32.8775 27 32.6375 26.952 32.4695 26.856C32.3015 26.748 32.1695 26.544 32.0735 26.244L31.7135 24.516C31.2335 24.96 30.7655 25.356 30.3095 25.704C29.8535 26.04 29.3735 26.328 28.8695 26.568C28.3655 26.796 27.8255 26.97 27.2495 27.09C26.6855 27.222 26.0555 27.288 25.3595 27.288C24.6515 27.288 23.9855 27.192 23.3615 27C22.7375 26.796 22.1915 26.496 21.7235 26.1C21.2675 25.704 20.9015 25.206 20.6255 24.606C20.3615 23.994 20.2295 23.274 20.2295 22.446C20.2295 21.726 20.4275 21.036 20.8235 20.376C21.2195 19.704 21.8615 19.11 22.7495 18.594C23.6375 18.078 24.7955 17.658 26.2235 17.334C27.6515 16.998 29.4035 16.806 31.4795 16.758V15.336C31.4795 13.92 31.1735 12.852 30.5615 12.132C29.9495 11.4 29.0555 11.034 27.8795 11.034C27.0875 11.034 26.4215 11.136 25.8815 11.34C25.3535 11.532 24.8915 11.754 24.4955 12.006C24.1115 12.246 23.7755 12.468 23.4875 12.672C23.2115 12.864 22.9355 12.96 22.6595 12.96C22.4435 12.96 22.2575 12.906 22.1015 12.798C21.9455 12.678 21.8135 12.534 21.7055 12.366L21.1295 11.34Z' stroke='white'/>
                <path d='M45.8461 18.09C46.4941 18.09 47.064 18 47.556 17.82C48.048 17.64 48.462 17.388 48.798 17.064C49.134 16.74 49.3861 16.356 49.5541 15.912C49.7221 15.456 49.806 14.958 49.806 14.418C49.806 13.302 49.464 12.414 48.78 11.754C48.108 11.094 47.1301 10.764 45.8461 10.764C44.5501 10.764 43.56 11.094 42.876 11.754C42.204 12.414 41.868 13.302 41.868 14.418C41.868 14.958 41.952 15.456 42.12 15.912C42.3 16.356 42.558 16.74 42.894 17.064C43.23 17.388 43.644 17.64 44.136 17.82C44.628 18 45.1981 18.09 45.8461 18.09ZM51.6241 27.99C51.6241 27.546 51.498 27.186 51.246 26.91C50.994 26.634 50.6521 26.418 50.2201 26.262C49.8001 26.106 49.308 25.998 48.744 25.938C48.18 25.866 47.5801 25.812 46.944 25.776C46.3201 25.74 45.684 25.704 45.036 25.668C44.388 25.632 43.764 25.572 43.164 25.488C42.492 25.8 41.94 26.196 41.508 26.676C41.088 27.144 40.878 27.696 40.878 28.332C40.878 28.74 40.98 29.118 41.184 29.466C41.4 29.826 41.724 30.132 42.156 30.384C42.588 30.648 43.128 30.852 43.776 30.996C44.436 31.152 45.21 31.23 46.098 31.23C46.962 31.23 47.736 31.152 48.42 30.996C49.104 30.84 49.68 30.618 50.148 30.33C50.628 30.042 50.994 29.7 51.246 29.304C51.498 28.908 51.6241 28.47 51.6241 27.99ZM54.882 9.48604V10.674C54.882 11.07 54.63 11.322 54.126 11.43L52.056 11.7C52.464 12.492 52.668 13.368 52.668 14.328C52.668 15.216 52.494 16.026 52.146 16.758C51.81 17.478 51.3421 18.096 50.7421 18.612C50.1421 19.128 49.4221 19.524 48.5821 19.8C47.7421 20.076 46.8301 20.214 45.8461 20.214C44.9941 20.214 44.19 20.112 43.434 19.908C43.05 20.148 42.756 20.406 42.552 20.682C42.348 20.946 42.246 21.216 42.246 21.492C42.246 21.924 42.42 22.254 42.768 22.482C43.128 22.698 43.5961 22.854 44.1721 22.95C44.7481 23.046 45.402 23.106 46.134 23.13C46.878 23.154 47.634 23.196 48.4021 23.256C49.182 23.304 49.938 23.394 50.67 23.526C51.414 23.658 52.074 23.874 52.65 24.174C53.226 24.474 53.688 24.888 54.036 25.416C54.396 25.944 54.576 26.628 54.576 27.468C54.576 28.248 54.378 29.004 53.982 29.736C53.598 30.468 53.04 31.116 52.308 31.68C51.576 32.256 50.676 32.712 49.608 33.048C48.552 33.396 47.358 33.57 46.026 33.57C44.694 33.57 43.5301 33.438 42.534 33.174C41.5381 32.91 40.71 32.556 40.05 32.112C39.39 31.668 38.892 31.152 38.556 30.564C38.232 29.988 38.07 29.382 38.07 28.746C38.07 27.846 38.352 27.084 38.916 26.46C39.48 25.836 40.254 25.338 41.238 24.966C40.698 24.726 40.266 24.408 39.942 24.012C39.63 23.604 39.474 23.058 39.474 22.374C39.474 22.11 39.522 21.84 39.618 21.564C39.714 21.276 39.858 20.994 40.05 20.718C40.254 20.43 40.5 20.16 40.788 19.908C41.076 19.656 41.412 19.434 41.796 19.242C40.896 18.738 40.194 18.072 39.69 17.244C39.186 16.404 38.934 15.432 38.934 14.328C38.934 13.44 39.102 12.636 39.438 11.916C39.786 11.184 40.266 10.566 40.878 10.062C41.49 9.54604 42.216 9.15004 43.056 8.87404C43.908 8.59804 44.8381 8.46004 45.8461 8.46004C46.6381 8.46004 47.376 8.55004 48.06 8.73004C48.744 8.89804 49.368 9.15004 49.932 9.48604H54.882Z' stroke='white'/>
                <path d='M70.1179 15.894C70.1179 15.15 70.0099 14.472 69.7939 13.86C69.5899 13.236 69.2839 12.702 68.8759 12.258C68.4799 11.802 67.9939 11.454 67.4179 11.214C66.8419 10.962 66.1879 10.836 65.4559 10.836C63.9199 10.836 62.7019 11.286 61.8019 12.186C60.9139 13.074 60.3619 14.31 60.1459 15.894H70.1179ZM72.7099 24.444C72.3139 24.924 71.8399 25.344 71.2879 25.704C70.7359 26.052 70.1419 26.34 69.5059 26.568C68.8819 26.796 68.2339 26.964 67.5619 27.072C66.8899 27.192 66.2239 27.252 65.5639 27.252C64.3039 27.252 63.1399 27.042 62.0719 26.622C61.0159 26.19 60.0979 25.566 59.3179 24.75C58.5499 23.922 57.9499 22.902 57.5179 21.69C57.0859 20.478 56.8699 19.086 56.8699 17.514C56.8699 16.242 57.0619 15.054 57.4459 13.95C57.8419 12.846 58.4059 11.892 59.1379 11.088C59.8699 10.272 60.7639 9.63604 61.8199 9.18004C62.8759 8.71204 64.0639 8.47804 65.3839 8.47804C66.4759 8.47804 67.4839 8.66404 68.4079 9.03604C69.3439 9.39604 70.1479 9.92404 70.8199 10.62C71.5039 11.304 72.0379 12.156 72.4219 13.176C72.8059 14.184 72.9979 15.336 72.9979 16.632C72.9979 17.136 72.9439 17.472 72.8359 17.64C72.7279 17.808 72.5239 17.892 72.2239 17.892H60.0379C60.0739 19.044 60.2299 20.046 60.5059 20.898C60.7939 21.75 61.1899 22.464 61.6939 23.04C62.1979 23.604 62.7979 24.03 63.4939 24.318C64.1899 24.594 64.9699 24.732 65.8339 24.732C66.6379 24.732 67.3279 24.642 67.9039 24.462C68.4919 24.27 68.9959 24.066 69.4159 23.85C69.8359 23.634 70.1839 23.436 70.4599 23.256C70.7479 23.064 70.9939 22.968 71.1979 22.968C71.4619 22.968 71.6659 23.07 71.8099 23.274L72.7099 24.444Z' stroke='white'/>
                <path d='M90.3561 5.23804C90.2481 5.41804 90.1281 5.55604 89.9961 5.65204C89.8761 5.73604 89.7261 5.77804 89.5461 5.77804C89.3421 5.77804 89.1021 5.67604 88.8261 5.47204C88.5501 5.26804 88.2021 5.04604 87.7821 4.80604C87.3741 4.55404 86.8761 4.32604 86.2881 4.12204C85.7121 3.91804 85.0101 3.81604 84.1821 3.81604C83.4021 3.81604 82.7121 3.92404 82.1121 4.14004C81.5241 4.34404 81.0261 4.62604 80.6181 4.98604C80.2221 5.34604 79.9221 5.77204 79.7181 6.26404C79.5141 6.74404 79.4121 7.26604 79.4121 7.83004C79.4121 8.55004 79.5861 9.15004 79.9341 9.63004C80.2941 10.098 80.7621 10.5 81.3381 10.836C81.9261 11.172 82.5861 11.466 83.3181 11.718C84.0621 11.958 84.8181 12.21 85.5861 12.474C86.3661 12.738 87.1221 13.038 87.8541 13.374C88.5981 13.698 89.2581 14.112 89.8341 14.616C90.4221 15.12 90.8901 15.738 91.2381 16.47C91.5981 17.202 91.7781 18.102 91.7781 19.17C91.7781 20.298 91.5861 21.36 91.2021 22.356C90.8181 23.34 90.2541 24.198 89.5101 24.93C88.7781 25.662 87.8721 26.238 86.7921 26.658C85.7241 27.078 84.5061 27.288 83.1381 27.288C81.4581 27.288 79.9341 26.988 78.5661 26.388C77.1981 25.776 76.0281 24.954 75.0561 23.922L76.0641 22.266C76.1601 22.134 76.2741 22.026 76.4061 21.942C76.5501 21.846 76.7061 21.798 76.8741 21.798C77.0301 21.798 77.2041 21.864 77.3961 21.996C77.6001 22.116 77.8281 22.272 78.0801 22.464C78.3321 22.656 78.6201 22.866 78.9441 23.094C79.2681 23.322 79.6341 23.532 80.0421 23.724C80.4621 23.916 80.9361 24.078 81.4641 24.21C81.9921 24.33 82.5861 24.39 83.2461 24.39C84.0741 24.39 84.8121 24.276 85.4601 24.048C86.1081 23.82 86.6541 23.502 87.0981 23.094C87.5541 22.674 87.9021 22.176 88.1421 21.6C88.3821 21.024 88.5021 20.382 88.5021 19.674C88.5021 18.894 88.3221 18.258 87.9621 17.766C87.6141 17.262 87.1521 16.842 86.5761 16.506C86.0001 16.17 85.3401 15.888 84.5961 15.66C83.8521 15.42 83.0961 15.18 82.3281 14.94C81.5601 14.688 80.8041 14.4 80.0601 14.076C79.3161 13.752 78.6561 13.332 78.0801 12.816C77.5041 12.3 77.0361 11.658 76.6761 10.89C76.3281 10.11 76.1541 9.15004 76.1541 8.01004C76.1541 7.09804 76.3281 6.21604 76.6761 5.36404C77.0361 4.51204 77.5521 3.75604 78.2241 3.09604C78.9081 2.43604 79.7421 1.90804 80.7261 1.51204C81.7221 1.11604 82.8621 0.918036 84.1461 0.918036C85.5861 0.918036 86.8941 1.14604 88.0701 1.60204C89.2581 2.05804 90.3021 2.71804 91.2021 3.58204L90.3561 5.23804Z' stroke='white'/>
                <path d='M110.595 8.76604V27H108.687C108.231 27 107.943 26.778 107.823 26.334L107.571 24.372C106.779 25.248 105.891 25.956 104.907 26.496C103.923 27.024 102.795 27.288 101.523 27.288C100.527 27.288 99.6454 27.126 98.8774 26.802C98.1214 26.466 97.4854 25.998 96.9694 25.398C96.4534 24.798 96.0634 24.072 95.7994 23.22C95.5474 22.368 95.4214 21.426 95.4214 20.394V8.76604H98.6254V20.394C98.6254 21.774 98.9374 22.842 99.5614 23.598C100.197 24.354 101.163 24.732 102.459 24.732C103.407 24.732 104.289 24.51 105.105 24.066C105.933 23.61 106.695 22.986 107.391 22.194V8.76604H110.595Z' stroke='white'/>
                <path d='M119.06 8.76604V27H115.856V8.76604H119.06ZM119.744 3.04204C119.744 3.35404 119.678 3.64804 119.546 3.92404C119.426 4.18804 119.258 4.42804 119.042 4.64404C118.838 4.84804 118.592 5.01004 118.304 5.13004C118.028 5.25004 117.734 5.31004 117.422 5.31004C117.11 5.31004 116.816 5.25004 116.54 5.13004C116.276 5.01004 116.042 4.84804 115.838 4.64404C115.634 4.42804 115.472 4.18804 115.352 3.92404C115.232 3.64804 115.172 3.35404 115.172 3.04204C115.172 2.73004 115.232 2.43604 115.352 2.16004C115.472 1.87204 115.634 1.62604 115.838 1.42204C116.042 1.20604 116.276 1.03804 116.54 0.918036C116.816 0.798037 117.11 0.738037 117.422 0.738037C117.734 0.738037 118.028 0.798037 118.304 0.918036C118.592 1.03804 118.838 1.20604 119.042 1.42204C119.258 1.62604 119.426 1.87204 119.546 2.16004C119.678 2.43604 119.744 2.73004 119.744 3.04204Z' stroke='white'/>
                <path d='M129.922 27.288C128.482 27.288 127.372 26.886 126.592 26.082C125.824 25.278 125.44 24.12 125.44 22.608V11.448H123.244C123.052 11.448 122.89 11.394 122.758 11.286C122.626 11.166 122.56 10.986 122.56 10.746V9.46804L125.548 9.09004L126.286 3.45604C126.322 3.27604 126.4 3.13204 126.52 3.02404C126.652 2.90404 126.82 2.84404 127.024 2.84404H128.644V9.12604H133.918V11.448H128.644V22.392C128.644 23.16 128.83 23.73 129.202 24.102C129.574 24.474 130.054 24.66 130.642 24.66C130.978 24.66 131.266 24.618 131.506 24.534C131.758 24.438 131.974 24.336 132.154 24.228C132.334 24.12 132.484 24.024 132.604 23.94C132.736 23.844 132.85 23.796 132.946 23.796C133.114 23.796 133.264 23.898 133.396 24.102L134.332 25.632C133.78 26.148 133.114 26.556 132.334 26.856C131.554 27.144 130.75 27.288 129.922 27.288Z' stroke='white'/>
                <path d='M149.325 15.894C149.325 15.15 149.217 14.472 149.001 13.86C148.797 13.236 148.491 12.702 148.083 12.258C147.687 11.802 147.201 11.454 146.625 11.214C146.049 10.962 145.395 10.836 144.663 10.836C143.127 10.836 141.909 11.286 141.009 12.186C140.121 13.074 139.569 14.31 139.353 15.894H149.325ZM151.917 24.444C151.521 24.924 151.047 25.344 150.495 25.704C149.943 26.052 149.349 26.34 148.713 26.568C148.089 26.796 147.441 26.964 146.769 27.072C146.097 27.192 145.431 27.252 144.771 27.252C143.511 27.252 142.347 27.042 141.279 26.622C140.223 26.19 139.305 25.566 138.525 24.75C137.757 23.922 137.157 22.902 136.725 21.69C136.293 20.478 136.077 19.086 136.077 17.514C136.077 16.242 136.269 15.054 136.653 13.95C137.049 12.846 137.613 11.892 138.345 11.088C139.077 10.272 139.971 9.63604 141.027 9.18004C142.083 8.71204 143.271 8.47804 144.591 8.47804C145.683 8.47804 146.691 8.66404 147.615 9.03604C148.551 9.39604 149.355 9.92404 150.027 10.62C150.711 11.304 151.245 12.156 151.629 13.176C152.013 14.184 152.205 15.336 152.205 16.632C152.205 17.136 152.151 17.472 152.043 17.64C151.935 17.808 151.731 17.892 151.431 17.892H139.245C139.281 19.044 139.437 20.046 139.713 20.898C140.001 21.75 140.397 22.464 140.901 23.04C141.405 23.604 142.005 24.03 142.701 24.318C143.397 24.594 144.177 24.732 145.041 24.732C145.845 24.732 146.535 24.642 147.111 24.462C147.699 24.27 148.203 24.066 148.623 23.85C149.043 23.634 149.391 23.436 149.667 23.256C149.955 23.064 150.201 22.968 150.405 22.968C150.669 22.968 150.873 23.07 151.017 23.274L151.917 24.444Z' stroke='white'/>
            </svg>
        </figure>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_fixed_cta($params = null) {
    echo "
        <div pagesuite fixed-cta
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        >
        <svg xmlns='http://www.w3.org/2000/svg'>
            <path fill='url(#paint0_linear)'/>
            <defs>
                <linearGradient id='paint0_linear' x1='236.5' y1='6.99998' x2='32.0001' y2='281.5' gradientUnits='userSpaceOnUse'>
                    <stop offset='0.257569' stop-color='#AB0E26'/>
                    <stop offset='1' stop-color='#061E33'/>
                </linearGradient>
            </defs>
        </svg>
            <div class='fixed-cta__btn'>
                <button pagesuite btn3
                ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
                >".php_trim_chars(($params['caption'] ?? 'caption'), 100)."</button>

                <button pagesuite symbol-btn
                ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
                class='display-none'>&#10005;</button>

                <div class='burger-nav'>
                    <div class='bar1'></div>
                    <div class='bar2'></div>
                    <div class='bar3'></div>
                </div>
            </div>
        </div>
        <script>
        let fixed_cta_btn = document.querySelector('[pagesuite][fixed-cta] button[btn3]');
        let fixed_cta_close_btn = document.querySelector('[pagesuite][fixed-cta] button[symbol-btn]');
        let fixed_cta_svg = document.querySelector('[pagesuite][fixed-cta] svg');
        let fixed_cta_svg_path = document.querySelector('[pagesuite][fixed-cta] svg path');
        let fixed_cta_svg_gradient = document.querySelector('[pagesuite][fixed-cta] svg linearGradient');
        let fixed_cta_burger_nav = document.querySelector('[pagesuite][fixed-cta] .burger-nav');
        
        function fixed_cta_callback() {
            fixed_cta_svg.classList.toggle('fixed-cta-svg-open');
            document.body.classList.toggle('overflow-hidden');
            fixed_cta_btn.classList.toggle('display-none');
            fixed_cta_close_btn.classList.toggle('display-none');
            if (fixed_cta_svg.classList.contains('fixed-cta-svg-open') == true) {
                fixed_cta_svg_gradient.setAttribute('x1', '2713.5');
                fixed_cta_svg_gradient.setAttribute('y1', '521.5');
                fixed_cta_svg_gradient.setAttribute('x2', '-231');
                fixed_cta_svg_gradient.setAttribute('y2', '3199');
            } else {
                setTimeout(() => {
                    fixed_cta_svg_gradient.setAttribute('x1', '236.5');
                    fixed_cta_svg_gradient.setAttribute('y1', '6.99998');
                    fixed_cta_svg_gradient.setAttribute('x2', '32.0001');
                    fixed_cta_svg_gradient.setAttribute('y2', '281.5');
                }, 250);
            }
        }
        
        fixed_cta_btn.addEventListener('click', fixed_cta_callback);
        fixed_cta_close_btn.addEventListener('click', fixed_cta_callback);
        
        fixed_cta_burger_nav.addEventListener('click', function () {
            fixed_cta_burger_nav.classList.toggle('toggled');
            fixed_cta_svg.classList.toggle('fixed-cta-svg-open');
            document.body.classList.toggle('overflow-hidden');
            if (fixed_cta_svg.classList.contains('fixed-cta-svg-open') == true) {
                fixed_cta_svg_gradient.setAttribute('x1', '2713.5');
                fixed_cta_svg_gradient.setAttribute('y1', '521.5');
                fixed_cta_svg_gradient.setAttribute('x2', '-231');
                fixed_cta_svg_gradient.setAttribute('y2', '3199');
            } else {
                setTimeout(() => {
                    fixed_cta_svg_gradient.setAttribute('x1', '236.5');
                    fixed_cta_svg_gradient.setAttribute('y1', '6.99998');
                    fixed_cta_svg_gradient.setAttribute('x2', '32.0001');
                    fixed_cta_svg_gradient.setAttribute('y2', '281.5');
                }, 250);
            }
        });
        </script>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_socmed_link($params = null) {
    echo "
        <a pagesuite socmed-link
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        href='".($params['href'] ?? NULL)."'
        target='_blank'
        >
            <img
            src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
            ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
            />
        </a>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_accordion_links($params = null) {
    echo "
        <div pagesuite accordion-link
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <label>".($params['label'] ?? 'label')."</label>
            <div class='accordion-link__links'>";

                if(empty($params['anchor_list'])) {
                    echo "anchor_list is empty";
                } else {
                    if(!is_array($params['anchor_list'])) {
                        echo $params['anchor_list'];
                    } else {
                        $anchor_list_init = 0;
                        $anchor_list_count = count($params['anchor_list']);
                        for($anchor_list_init; $anchor_list_init < $anchor_list_count; $anchor_list_init++) {
                            echo $params['anchor_list'][$anchor_list_init];
                        }
                    }
                }

            echo "</div>
        </div>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_device_morph() {
    echo "
        <div pagesuite device-morph>
            <div class='device-position'>
                <div class='device'>
                    <div class='left'></div>
                    <div class='right'></div>

                    <div class='screen'>
                        <img src='/theme_pagesuite/img/morphing/notch.svg'>
                        <img src='/theme_pagesuite/img/morphing/phonescreen.svg'>
                        <img src='/theme_pagesuite/img/morphing/ipadscreen.svg'>
                        <img src='/theme_pagesuite/img/morphing/imacscreen.svg'>
                    </div>

                    <div class='settings'></div>

                    <div class='stand'>
                        <img src='/theme_pagesuite/img/morphing/stand.svg'>
                    </div>
                </div>
            </div>
        </div>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_card1($params = null) {
    echo "
        <figure pagesuite c1
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <div class='figimg'>
                <img 
                src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
                ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
                />
            </div>
            <figcaption class='figcaption'>
                <h1>".php_trim_chars(($params['header'] ?? 'header'), 17)."</h1>
                <p>".php_trim_chars(($params['caption'] ?? 'caption'), 120)."</p>
            </figcaption>
        </figure>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_card2($params = null) {
    echo "
        <aside pagesuite c2
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <h1>".php_trim_chars(($params['header'] ?? 'header'), 10)."</h1>
            <p>".php_trim_chars(($params['caption'] ?? 'caption'), 21)."</p>
        </aside>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_card3($params = null) {
    echo "
        <figure pagesuite c3
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <div class='figimg'>
                <img
                src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
                ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
                />
            </div>
            <figcaption class='figcaption'>
                <h1>".php_trim_chars(($params['header'] ?? 'header'), 20)."</h1>
                <p>".php_trim_chars(($params['caption'] ?? 'caption'), 100)."</p>
            </figcaption>
        </figure>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_card4($params = null) {
    echo "
        <article pagesuite c4
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <div class='header'>
                <img
                src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
                ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
                />
            </div>
            <div class='body'>
                <div class='body--container'>
                    <div class='container--top'>
                        <p>".php_trim_chars(($params['caption'] ?? 'caption'), 220)."</p>
                    </div>
                    <div class='container--bottom'>
                        <p>".php_trim_chars(($params['header1'] ?? 'header1'), 38)."</p>
                        <p>".php_trim_chars(($params['header2'] ?? 'header2'), 38)."</p>
                        <p>".php_trim_chars(($params['header3'] ?? 'header3'), 38)."</p>
                    </div>
                </div>
            </div>
        </article>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_card5($params = null) {
    echo "
        <article pagesuite c5
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <div class='header'>
                <img
                src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
                ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
                />
            </div>
            <div class='body'>
                <div class='body--container'>
                    <div class='container--top'>
                        <p>".php_trim_chars(($params['caption'] ?? 'caption'), 220)."</p>
                    </div>
                    <div class='container--bottom'>
                        <p>".php_trim_chars(($params['header1'] ?? 'header1'), 38)."</p>
                        <p>".php_trim_chars(($params['header2'] ?? 'header2'), 38)."</p>
                        <p>".php_trim_chars(($params['header3'] ?? 'header3'), 38)."</p>
                    </div>
                </div>
            </div>
        </article>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_btn1($params = null) {
    echo "
        <button pagesuite btn1
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >".php_trim_chars(($params['caption'] ?? 'caption'), 100)."</button>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_btn2($params = null) {
    echo "
        <button pagesuite btn2
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >".php_trim_chars(($params['caption'] ?? 'caption'), 100)."</button>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_btn3($params = null) {
    echo "
        <button pagesuite btn3
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >".php_trim_chars(($params['caption'] ?? 'caption'), 100)."</button>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_symbol_btn($params = null) {
    echo "
        <button pagesuite symbol-btn
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >".php_trim_chars(($params['caption'] ?? 'caption'), 100)."</button>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_newsletter($params = null) {
    echo "
        <form pagesuite newsletter method='POST' action='".($params['action'] ?? 'javascript:;')."'
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >

            <input type='text' id='pagesuite_newsletter_textfield_id' class='pagesuite_newsletter_textfield_class' name='pagesuite_newsletter_textfield_name'
            placeholder='".php_trim_chars(($params['textfield_placeholder'] ?? 'Sign up to our Newsletter'), 36)."'
            >

            <input type='image' src='/theme_pagesuite/img/emailicon.svg'
            alt='Submit' 
            id='pagesuite_newsletter_submit_id' 
            class='pagesuite_newsletter_submit_class' 
            name='pagesuite_newsletter_submit_name'>
        </form>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_headline($params = null) {
    echo "
        <aside pagesuite
        ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
        ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
        ".(empty($params['breakpoint']) ? 'component-breakpoint=\'768, 1366, 1920\'' : 'component-breakpoint=\''.$params['breakpoint'].'\'')."
        ".($params['attr_addons'] ?? NULL)."
        >
            <h1>".($params['header'] ?? 'header')."</h1>
            <p>".($params['caption'] ?? 'caption')."</p>
        </aside>
    ";
}

/* ----------------------------------------------------------------------------------------------------------------------- */

function pagesuite_section_decor($params = null) {
    echo "<img pagesuite 
    ".(empty($params['id']) ? NULL : 'id=\''.$params['id'].'\'')."
    ".(empty($params['class']) ? NULL : 'class=\''.$params['class'].'\'')."
    src='".($params['img_src'] ?? 'img_src')."'>";
}