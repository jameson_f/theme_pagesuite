<?php

/*

JavaScript execution hierarchy:
async or the first time it gets parsed
defer
document - DOMContentLoaded
window - onload

• to use the themed components, add the theme folder in the root directory
  and add the code below inside the head of your index.html

<head>
    <?php require 'theme_themename/framework.php'; ?>
</head>

• themename is the name of the theme you wish to use

------------------------------------------------------
------------------------------------------------------
    "" => "",
------------------------------------------------------
------------------------------------------------------

widget attributes:
    --  No need to echo, it is echoed automatically
    --  Events must be done via JS
    --  It is possible to use multiple themes

    theme_widgetName([
        "id" => "all widgets have this property - the id of the component",
        "class" => "all widgets have this property - the class of the component",
        "breakpoint" => "all widgets have this property - breakpoints for mobile, tablet, desktop view. Will show a specific view upto the given breakpoint - mobile first approach",
        "attr_addons" => "all widgets have this property - custom attributes like data-src='null' aria-hidden='true'",
    ]);

--------------------------------------------------------------------------------------------------------------------------

SAMPLE:

    pagesuite_c1([
        "class" => "genericClass",
        "id" => "uniqId",
        "breakpoint" => "500, 1000, 1500",
        "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
        "img_src" => "img/card1c.svg",
        "img_alt" => "Image alt text",
        "header" => "EDITION",
        "caption" => "Crafted editions developed using existing content, offering publishers new way to grow revenue",
    ]);

*/

/* ----------------------------------------------------------------------------------------------------------------------- */

// SAMPLE WIDGET WITH ACTUAL CONTENT

//? pagesuite_logo
//?  --  pagesuite logo widget
//* img_src = the image path (relative, absolute, or URL)
//* img_alt = the image description (no character limit)
// todo
// <figure>
//     <img src="img_src" alt="img_alt">
// </figure>
pagesuite_logo([
    "id" => "uniqId",
    "class" => "bg-2c qq",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "img_src" => "img/psicon.png",
    "img_alt" => "Image alt text",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_card1
//?  --  column format card that contains image, header, and caption
//* img_src = the image path (relative, absolute, or URL)
//* img_alt = the image description (no character limit)
//* header = the widget header (17 characters max)
//* caption = the widget text (120 characters max)
// todo
// <figure>
//     <div class="figimg">
//         <img src="img_src" alt="img_alt">
//     </div>
//     <figcaption class="figcaption">
//         <h1>header</h1>
//         <p>caption</p>
//     </figcaption>
// </figure>
pagesuite_card1([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "img_src" => "img/card1b.svg",
    "img_alt" => "Image alt text",
    "header" => "EDITION",
    "caption" => "Crafted editions developed using existing content, offering publishers new way to grow revenue",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_card2
//?  --  text widget
//?  --  contains <h1> and <p>
//* header = <h1> (10 characters max)
//* caption = <p> (21 characters max)
// todo
// <aside>
//     <h1>header</h1>
//     <p>caption</p>
// </aside>
pagesuite_card2([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "header" => "35,032",
    "caption" => "PAGE PROCESSED DAILY",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_card3
//?  --  row format card that contains image, header, and caption
//* img_src = the image path (relative, absolute, or URL)
//* img_alt = the image description (no character limit)
//* header = the widget header (20 characters max)
//* caption = the widget text (100 characters max)
// todo
// <figure>
//     <div class="figimg">
//         <img src="img_src" alt="img_alt">
//     </div>
//     <figcaption class="figcaption">
//         <h1>header</h1>
//         <p>caption</p>
//     </figcaption>
// </figure>
pagesuite_card3([
    "id" => "uniqId",
    "class" => "bg-2c",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "img_src" => "img/card3a.png",
    "img_alt" => "Image alt text",
    "header" => "Increase Subscriber",
    "caption" => "Crafted editions developed using existing content, offering publishers new way to grow revenue",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_card4
//?  --  testimonial card widget
//?  --  shows content when swiper-slide-active class is present
//* img_src = the image path (relative, absolute, or URL)
//* img_alt = the image description (no character limit)
//* caption = the testimonial text (220 characters max)
//* header1 = identification text. name, company, or position (38 characters max)
//* header2 = identification text. name, company, or position (38 characters max)
//* header3 = identification text. name, company, or position (38 characters max)
// todo
// <article>
//     <div class="header">
//         <img src="img_src" alt="img_alt">
//     </div>
//     <div class="body">
//         <div class="body--container">
//             <div class="container--top">
//                 <p>caption</p>
//             </div>
//             <div class="container--bottom">
//                 <p>header1</p>
//                 <p>header2</p>
//                 <p>header3</p>
//             </div>
//         </div>
//     </div>
// </article>
pagesuite_card4([
    "id" => "uniqId",
    "class" => "swiper-slide-active",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "img_src" => "img/fijitimes.png",
    "img_alt" => "Image alt text",
    "caption" => "We selected PageSuite because it had the most features and was the most user-friendly. The goal of the app is convenience, easy to read and with regular updates, the news as it happens.",
    "header1" => "THE FIJI TIMES",
    "header2" => "HANK ARTS",
    "header3" => "GENERAL MANAGER/PUBLISHER",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_card5
//?  --  testimonial card widget
//?  --  shows content when hovered
//* img_src = the image path (relative, absolute, or URL)
//* img_alt = the image description (no character limit)
//* caption = the testimonial text (220 characters max)
//* header1 = identification text. name, company, or position (38 characters max)
//* header2 = identification text. name, company, or position (38 characters max)
//* header3 = identification text. name, company, or position (38 characters max)
// todo
// <article>
//     <div class="header">
//         <img src="img_src" alt="img_alt">
//     </div>
//     <div class="body">
//         <div class="body--container">
//             <div class="container--top">
//                 <p>caption</p>
//             </div>
//             <div class="container--bottom">
//                 <p>header1</p>
//                 <p>header2</p>
//                 <p>header3</p>
//             </div>
//         </div>
//     </div>
// </article>
pagesuite_card5([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "img_src" => "img/tribune.png",
    "img_alt" => "Image alt text",
    "caption" => "We selected PageSuite because it had the most features and was the most user-friendly. The goal of the app is convenience, easy to read and with regular updates, the news as it happens.",
    "header1" => "THE FIJI TIMES",
    "header2" => "HANK ARTS",
    "header3" => "GENERAL MANAGER/PUBLISHER",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_btn1
//?  --  button widget
//* caption = the button text (no character limit)
// todo
// <button>Button Text</button>
pagesuite_btn1([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "caption" => "Contact Us",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_btn2
//?  --  button widget
//* caption = the button text (no character limit)
// todo
// <button>Button Text</button>
pagesuite_btn2([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "caption" => "Call to Action",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_btn3
//?  --  button widget
//* caption = the button text (no character limit)
// todo
// <button>Button Text</button>
pagesuite_btn3([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "caption" => "Call to Action",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_symbol_btn
//?  --  button widget that only contains symbols
//?  --  this button's styling depends on its class
//* caption = the button text (no character limit)
// todo
// <button>Button Text</button>
pagesuite_symbol_btn([
    "id" => "uniqId",
    "class" => "genericClass font-size-32",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "caption" => "&#10005;",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_newsletter
//?  --  form widget
//* action = form action, the page where the form data will be sent to
//*          - page and url are allowed
//*          - if action is empty, it is understood that form data will be sent via ajax
//*          - use javascript to send form data via ajax
//* img_src = the send button icon (must be 48x48 pixels)
//* textfield_placeholder = the placeholder for text field (36 characters max)
// todo
// <form action="action">
//     <input type="text" placeholder="textfield_placeholder">
//     <input type="image" src="img_src" alt="Submit">
// </form>
pagesuite_newsletter([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "action" => "index.php?q=abcdefg",
    "img_src" => "img/emailicon.png",
    "textfield_placeholder" => "TextField Placeholder",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_headline
//?  --  contains h1 and p
//?  --  this widget has no default styling
//?  --  styling of this widget will depend on its class
//* header = <h1> (no character limit)
//* caption = <p> (no character limit)
// todo
// <aside>
//     <h1>header</h1>
//     <p>caption</p>
// </aside>
pagesuite_headline([
    "id" => "uniqId",
    "class" => "homepage-hero-headline red",
    "breakpoint" => "550, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "header" => "Market-Leading Digital Solutions",
    "caption" => "Maximise the value of your content on multiple platforms",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_section_decor
//? --  add section decoration
//? --  contains image in an absolute position
//? --  everything is controlled by class
//* class="section-decor-top" = absolute; top; left;
//* class="section-decor-bottom" = absolute; bottom; right;
//* class="flip-top-right" = flip the image horizontally
//* class="flip-bottom-right" = flip the image horizontally and vertically
//* class="flip-bottom-left" = flip the image vertically
// todo
// <img src='img_src'>
pagesuite_section_decor([
    "img_src" => "theme_pagesuite/img/wing1.svg",
    "class" => "section-decor-bottom flip-bottom-left",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_image_link
//? -- social media image inside anchor
//* href = the link to go to
//* img_src = image source
//* img_alt = image alt text
// todo
// <div>
//     <label>Label</label>
//     <div class='accordion-link__links'>
//         <a href="">Link 1</a>
//         <a href="">Link 2</a>
//         <a href="">Link 3</a>
//     </div>
// </div>
pagesuite_socmed_link([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "href" => "https://99porings.com",
    "img_src" => "img/linkedin.png",
    "img_alt" => "linkedin icon",
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_accordion_links
//? -- accordion that contain links
//* label = the visible part of accordion
//* anchor_list = the list that will show up when label is clicked
// todo
pagesuite_accordion_links([
    "id" => "uniqId",
    "class" => "genericClass",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "label" => "Accordion Header",
    "anchor_list" => [
        "<a href=''>Link 1</a>",
        "<a href=''>Link 2</a>",
        "<a href=''>Link 3</a>",
    ],
]);

/* ----------------------------------------------------------------------------------------------------------------------- */

//? pagesuite_fixed_cta
//? --  Call to action that is in fixed position
//? --  This widget follows the scroll
//* caption = caption of the button
pagesuite_fixed_cta([
    "id" => "cta-uniqid",
    "class" => "cta-generic-class",
    "breakpoint" => "500, 1000, 1500",
    "attr_addons" => "data-src='null' aria-hidden='true' additional-data='123456789'",
    "caption" => "Contact Us",
]);