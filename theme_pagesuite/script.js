// JavaScript execution hierarchy:
// async or the first time it gets parsed
// defer
// DOMContentLoaded
// onload

class THEME_PAGESUITE {

    constructor(themeName) {
        // var assignments
        // this.css = this.qs('head'); // get <head></head> tag - can be removed if you want to add CSS separately
        this.theme = this.qs('[' + themeName + '] [component-breakpoint]'); // parent theme [theme] [component]
        this.themeInline = this.qs('[' + themeName + '][component-breakpoint]'); // inline theme [theme][component]

        // lazy load assignments
        this.lazyImg = this.qs('[' + themeName + '] img[data-src]'); // get theme images in preparation for lazy loading [theme] img
        // this.gbc = this.target.getBoundingClientRect();

        // execute methods
        // this._injectCss(themeName); // inject theme css - can be removed if you want to add CSS separately
        this._parentThemeOnload(); // sets the initial state of components depending on viewport - onload
        this._inlineThemeOnload(); // sets the initial state of components depending on viewport - onload
        this._parentThemeOnresize(); // change the state of components depending on viewport - onresize
        this._inlineThemeOnresize(); // change the state of components depending on viewport - onresize
    }

    _lazyLoad() {
        let i = 0;
        let n = this.lazyImg.length
        let windowHeight = window.innerHeight;
        for (i; i < n; i++) {
            let boundingClientRect = this.lazyImg[i].getBoundingClientRect();
            if (this.lazyImg[i].hasAttribute("data-src") && boundingClientRect.top < windowHeight) {
                this.lazyImg[i].setAttribute("src", this.lazyImg[i].getAttribute("data-src"));
                this.lazyImg[i].removeAttribute("data-src");
            }
        }
    }

    // _injectCss(themeName) { // inject theme css - can be removed if you want to add CSS separately
    //     this.css[0].insertAdjacentHTML('afterbegin', '<link rel="stylesheet" href="theme_' + themeName + '/style.css">');
    // }

    _parentThemeOnload() { // sets the initial state of components depending on viewport - onload
        let i = 0;
        const n = this.theme.length
        for (i; i < n; i++) {
            if (window.innerWidth <= this.getBreakpoint(this.theme[i])[0]) {
                this.setViewport(this.theme[i], 'phone');
            } else if (window.innerWidth <= this.getBreakpoint(this.theme[i])[1]) {
                this.setViewport(this.theme[i], 'tablet');
            } else if (window.innerWidth <= this.getBreakpoint(this.theme[i])[2]) {
                this.setViewport(this.theme[i], 'desktop');
            } else {
                this.setViewport(this.theme[i], 'desktop');
            }
        }
    }

    _inlineThemeOnload() { // sets the initial state of components depending on viewport - onload
        let i = 0;
        const n = this.themeInline.length
        for (i; i < n; i++) {
            if (window.innerWidth <= this.getBreakpoint(this.themeInline[i])[0]) {
                this.setViewport(this.themeInline[i], 'phone');
            } else if (window.innerWidth <= this.getBreakpoint(this.themeInline[i])[1]) {
                this.setViewport(this.themeInline[i], 'tablet');
            } else if (window.innerWidth <= this.getBreakpoint(this.themeInline[i])[2]) {
                this.setViewport(this.themeInline[i], 'desktop');
            } else {
                this.setViewport(this.themeInline[i], 'desktop');
            }
        }
    }

    _parentThemeOnresize() { // change the state of components depending on viewport - onresize
        let i = 0;
        const n = this.theme.length;
        window.addEventListener("resize", _ => {
            this.forLoop(i, n, (i) => {
                if (window.innerWidth <= this.getBreakpoint(this.theme[i])[0]) {
                    this.setViewport(this.theme[i], 'phone');
                } else if (window.innerWidth <= this.getBreakpoint(this.theme[i])[1]) {
                    this.setViewport(this.theme[i], 'tablet');
                } else if (window.innerWidth <= this.getBreakpoint(this.theme[i])[2]) {
                    this.setViewport(this.theme[i], 'desktop');
                } else {
                    this.setViewport(this.theme[i], 'desktop');
                }
            });
        });
    }

    _inlineThemeOnresize() { // change the state of components depending on viewport - onresize
        let i = 0;
        const n = this.themeInline.length;
        window.addEventListener("resize", _ => {
            this.forLoop(i, n, (i) => {
                if (window.innerWidth <= this.getBreakpoint(this.themeInline[i])[0]) {
                    this.setViewport(this.themeInline[i], 'phone');
                } else if (window.innerWidth <= this.getBreakpoint(this.themeInline[i])[1]) {
                    this.setViewport(this.themeInline[i], 'tablet');
                } else if (window.innerWidth <= this.getBreakpoint(this.themeInline[i])[2]) {
                    this.setViewport(this.themeInline[i], 'desktop');
                } else {
                    this.setViewport(this.themeInline[i], 'desktop');
                }
            });
        });
    }

    qs(selector) { // query selector method
        return document.querySelectorAll(selector);
    }

    forLoop(floor, ceil, callback) { // for loop method
        for (floor; floor < ceil; floor++) {
            callback(floor);
        }
    }

    getBreakpoint(element) { // get component breakpoint
        return element.getAttribute('component-breakpoint').split(',');
    }

    setViewport(element, viewport) { // set to phone, tablet or desktop depending on the value of component-breakpoint
        element.setAttribute('viewport-of', viewport);
    }

}

let theme_pagesuite = null;

document.addEventListener('DOMContentLoaded', function () {
    theme_pagesuite = new THEME_PAGESUITE('pagesuite');
});
document.addEventListener('scroll', function () {
    theme_pagesuite._lazyLoad();
});
window.addEventListener('load', function () {
    theme_pagesuite._lazyLoad();
});
window.addEventListener('resize', function () {
    theme_pagesuite._lazyLoad();
});