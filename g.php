<?php    
function card1($params = null) {
    echo "
    <figure>
        <div class='image-placeholder'></div>
        <figcaption>
            <h1>".$params['header']."</h1>
            <p>".$params['caption']."</p>
        </figcaption>
    </figure>
    ";
}

function card2($params = null) {
    echo "
    <figure class='card2'>
        <div class='main'>
            <div class='image-placeholder1'></div>
        </div>
        <figcaption>
            <h1>".$params['header']."</h1>
            <p>".$params['caption']."</p>
        </figcaption>
    </figure>
    ";
}

function card3($params = null) {
    echo "
    <figure class='card3'>
        <div class='main'>
            <div class='image-placeholder1'></div>
        </div>
        <figcaption>
            <h1>".$params['header']." <b>".$params['header_bold']."</b></h1>
            <p>".$params['caption']."</p>
        </figcaption>
    </figure>
    ";
}

function card4($params = null) {
    echo "
    <div class='card4'>
        <div class='small_box'>
            <h2>".$params['header_small_box']."</h2>
        </div>
        <div class='bigger_box'>
            <p id='red_text'>".$params['red_text']."</p>
            <h1>".$params['header']."</h1>
            <p>".$params['caption']."</p>
            <div class='bottom_box'><span>".$params['bottom_text']."</span></div>
        </div>
    </div>
    ";
}

?>
 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Garima Page</title>
    <style>
    body {
        background: #aaa;
    }
        figure{ 
            width: 320px;
            height: 332px;
            background: #FFFFFF 0% 0% no-repeat padding-box;
            box-shadow: 3px 3px 10px #2222224D;
            border-radius: 15px;
            opacity: 1;
            position: relative;
            top: 50px;
                display: inline-flex;
        }
        .image-placeholder {
            width: 98px;
            height: 98px;
            background: transparent url(img/Group 4830.png) 0% 0% no-repeat padding-box;
            opacity: 1;
            background-color: #202BAC;
            position: absolute;
            top: -39px;
            border-radius: 50%;
            left: 113px;
        }
        figcaption {
            position: absolute;
            top: 76px;
            text-align: center;
            letter-spacing: 0px;
            opacity: 1;
            padding-left: 15px;
            padding-right: 15px;
        }
        figcaption h1{
            font: Bold 25px/32px Poppins;
            color: #061E33;
            text-transform: uppercase;
        }

        figcaption p{
            font: Regular 18px/28px Lato;
            color: #2C2C2C;
        }

        /*----------------------card2 css---------------------------------*/
        figure.card2 {
            padding-top: 61px;
        }
        .main{
            width: 100%;
        }
        .image-placeholder1 {
            width: 71px;
            height: 71px;
            opacity: 1;
            border-radius: 50%;
            background-color: #D74850;
            margin: 0 auto;
        }
        .card2 figcaption{
            position: absolute;
            top: 76px;
            text-align: center;
            letter-spacing: 0px;
            opacity: 1;
            width: 198px;
            height: 84px;
            padding: 61px;
            margin: 0 auto;
        }

         /*----------------------card3 css---------------------------------*/
        figure.card3 {
            padding-top: 25px;
            width: 440px;
            height: 250px;
            background: #FFFFFF 0% 0% no-repeat padding-box;
            box-shadow: 3px 3px 10px #2222224D;
            border-radius: 15px;
            opacity: 1;
            padding-left: 50px;
            padding-bottom: 25px;
            padding-right: 28px;
        }
        .main{
            width: 100%;
        }
        .card3 .image-placeholder1 {
            margin: 0;
        }
        .card3 figcaption {
            position: absolute;
            top: 76px;
            letter-spacing: 0px;
            opacity: 1;
            margin: 0;
            padding-top: 25px;
            text-align: left;
        }
        .card3 figcaption h1 {
            font: 25px/32px Poppins;
            color: #061E33;
            text-transform: capitalize;
        }

         /*----------------------card4 css---------------------------------*/
        .card4 {
            width: 651px;
            height: 295px;
            opacity: 1;
            position: relative;
            margin-top: 50px;
        }
        .small_box {
            width: 176px;
            background: #FFFFFF 0% 0% no-repeat padding-box;
            box-shadow: 5px 5px 10px #0000004D;
            border-radius: 15px;
            opacity: 1;
            position: absolute;
            top: 65px;
            z-index: 1;
        }   
        .small_box h2 {
            padding: 69px 17px;
            text-align: center;
            font-size: 20px;
           
        }
        .bigger_box {
            width: 549px;            
            background: #FFFFFF 0% 0% no-repeat padding-box;
            box-shadow: 3px 3px 6px #22222240;
            border-radius: 15px;
            opacity: 1;
            position: relative;
            left: 122px;
            z-index: 0;
            padding: 68px 112px;
        }
        #red_text{
            width: 136px;
            height: 22px;
            text-align: left;
            font-size:  18px;
            letter-spacing: 0px;
            color: #CC2027;
            opacity: 1;
        }
        .bottom_box {
            position: absolute;
            right: 0;
            bottom: 0;
            background-color: #061E33;
            width: 147px;
            text-align: center;
            padding: 10px;
            border-top-left-radius: 15px;
        }
        
        .bottom_box span {
            color: #ccc;
        }
    </style>


</head>
<body>
    <?php
        card1([
            'header' => 'EDITION',
            'caption' => 'Crafted editions developed using existing content, offering publishers new way to grow revenue',
        ]);
        card1([
            'header' => 'LIVE',
            'caption' => 'A regularly updating live news experience, either from a feed of existing content or within the Content Management System.',
        ]);
        card1([
            'header' => 'Web',
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed ',
        ]);

        /*-----------card2------------------------------------*/

        card2([
            'header' => '3,300+',
            'caption' => 'Apps in store ',
        ]);


        /*-----------card3------------------------------------*/

        card3([
            'header' => 'Feature',
            'header_bold' => 'Title',
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ',
        ]);

         /*-----------card3------------------------------------*/

         card3([
            'header' => 'Feature',
            'header_bold' => 'Title',
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ',
        ]);

          /*-----------card3------------------------------------*/

        card4([
            'header_small_box' =>'The Fiji Times',
            'red_text' => '“General Manager',
            'header' => 'Hank Arts',            
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ',
            'bottom_text' => 'View Project',
        ]);
    ?>
   
</body>
</html>