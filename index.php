<?php
function qq($params = null) {
    echo "
    <div pagesuite>
    <img
    src='/theme_pagesuite/img/loader.svg' data-src='".($params['img_src'] ?? 'img_src')."' ".(empty($params['img_src']) ? 'alt=\'img_src & img_alt\'' : NULL)."
    ".(empty($params['img_alt']) ? NULL : 'alt=\''.$params['img_alt'].'\'')."
    />
    </div>
    ";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require 'theme_pagesuite/framework.php';
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        body {
            background: #eee;
            display: flex;
            justify-content: space-evenly;
            align-items: center;
        }
    </style>
</head>
<body>
    <?php
    pagesuite_card1([
        'img_src' => 'https://99porings.com/exchange/img/global/99ex--small.png',
        'img_alt' => 'qq',
    ]);

    pagesuite_card5([
        'img_src' => 'https://99porings.com/exchange/img/global/99ex--small.png',
        'img_alt' => 'qq',
    ]);

    qq([
        'img_src' => 'https://99porings.com/exchange/img/global/99ex--small.png',
        'img_alt' => 'qq',
    ]);
    ?>
</body>
</html>